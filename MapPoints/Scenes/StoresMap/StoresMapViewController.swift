//
//  StoresMapViewController.swift
//  MapPoints
//
//  Created by Matus Littva on 02/07/2021.
//

import UIKit
import MapKit

final class StoresMapViewController: UIViewController, Alertable {
    
    @IBOutlet private var mapView: MKMapView!
    @IBOutlet private var mapTypeButton: BouncingButton!
    @IBOutlet private var userLocationButton: BouncingButton!
    
    private var viewModel: StoresMapConfigurable!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        mapView.mapType == .standard ? .default : .lightContent
    }
    
    convenience init(viewModel: StoresMapConfigurable) {
        self.init()
        self.viewModel = viewModel
        
        bindViewModelOutput()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupMapView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.viewWillAppear()
    }
    
    func startNavigation() {
        guard let destinationStore = mapView.selectedAnnotations.first else { return }
        
        let source = MKMapItem(placemark: MKPlacemark(coordinate: mapView.userLocation.coordinate))
        source.name = mapView.userLocation.title
        
        let destination = MKMapItem(placemark: MKPlacemark(coordinate: destinationStore.coordinate))
        destination.name = destinationStore.title ?? ""
        
        MKMapItem.openMaps(
            with: [source, destination],
            launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeWalking,
                            MKLaunchOptionsMapTypeKey: mapView.mapType.rawValue]
        )
    }
    
    func deselectSelectedStore() {
        mapView.deselectAnnotation(mapView.selectedAnnotations.first, animated: true)
    }
    
    // MARK: - Private
    
    private func bindViewModelOutput() {
        viewModel.errorHandler = { [weak self] title, message in
            self?.presentAlert(title: title, message: message)
        }
        viewModel.annotationsHandler = { [weak self] annotations in
            let mapAnnotations: [MKPointAnnotation] = annotations.map {
                let annotation = MKPointAnnotation()
                annotation.title = $0.title
                annotation.subtitle = $0.subtitle
                annotation.coordinate = CLLocationCoordinate2D(latitude: $0.latitude, longitude: $0.longitude)
                return annotation
            }
            self?.mapView.addAnnotations(mapAnnotations)
            self?.mapView.showAnnotations(mapAnnotations, animated: true)
        }
        viewModel.userCoordinatesHandler = { [weak self] latitude, longitude in
            self?.mapView.setRegion(MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: latitude, longitude: longitude), span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)), animated: false)
        }
    }
    
    private func setupMapView() {
        mapView.delegate = self
        mapView.showsUserLocation = true
        mapView.register(StoreAnnotationView.self, forAnnotationViewWithReuseIdentifier: String(describing: StoreAnnotationView.self))
    }
    
    // MARK: - Actions
    
    @IBAction private func changeMapType(_ sender: Any) {
        mapView.mapType = mapView.mapType == .standard ? .hybrid : .standard
        mapTypeButton.isSelected = mapView.mapType == .hybrid
        setNeedsStatusBarAppearanceUpdate()
    }
    
    @IBAction private func showCurrentLocation(_ sender: Any) {
        mapView.setCenter(mapView.userLocation.coordinate, animated: true)
    }
}

// MARK: - MapView Delegate

extension StoresMapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        userLocationButton.isSelected = CLLocation(coordinate: mapView.centerCoordinate).isEqual(to: mapView.userLocation.coordinate)
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let coordinate = view.annotation?.coordinate else { return }
        
        let mapPoint = MKMapPoint(coordinate)
        var mapRect = mapView.visibleMapRect
        
        mapRect.origin.x = mapPoint.x - mapRect.width / 2
        mapRect.origin.y = mapPoint.y - mapRect.height / 3
        
        mapView.setVisibleMapRect(mapRect, animated: true)
        viewModel.annotationSelected(latitude: coordinate.latitude, longitude: coordinate.longitude)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }
        
        return mapView.dequeueReusableAnnotationView(withIdentifier: String(describing: StoreAnnotationView.self))
    }
}
