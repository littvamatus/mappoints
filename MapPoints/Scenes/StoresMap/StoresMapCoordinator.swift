//
//  StoresMapCoordinator.swift
//  MapPoints
//
//  Created by Matus Littva on 02/07/2021.
//

import UIKit

final class StoresMapCoordinator: Coordinator {
    
    private let storesProvider: StoresProvider
    
    private var rootViewController: StoresMapViewController!
    
    init(storesProvider: StoresProvider) {
        self.storesProvider = storesProvider
    }
    
    func start() -> UIViewController {
        let viewModel = StoresMapViewModel(storesProvider: storesProvider)
        let storesMapViewController = StoresMapViewController(viewModel: viewModel)
        viewModel.coordinatorDelegate = self
        rootViewController = storesMapViewController
        return rootViewController
    }
}

// MARK: - StoresMap Coordinator Delegate

extension StoresMapCoordinator: StoresMapViewModelCoordinatorDelegate {
    
    func showDetail(store: Store, distance: Double) {
        let viewModel = StoreDetailViewModel(store: store, distance: distance)
        let detailController = StoreDetailViewController(viewModel: viewModel)
        viewModel.coordinatorDelegate = self
        rootViewController.presentPanModal(detailController)
    }
}

// MARK: - StoreDetail Coordinator Delegate

extension StoresMapCoordinator: StoreDetailViewModelCoordinatorDelegate {

    func navigateToSelectedStore() {
        rootViewController.startNavigation()
    }
    
    func storeDetailWillDismiss() {
        rootViewController.deselectSelectedStore()
    }
}
