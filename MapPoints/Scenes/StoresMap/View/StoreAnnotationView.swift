//
//  StoreAnnotationView.swift
//  MapPoints
//
//  Created by Matus Littva on 04/07/2021.
//

import MapKit

final class StoreAnnotationView: MKMarkerAnnotationView {
    
    override var annotation: MKAnnotation? {
        willSet {
            setupView()
        }
    }
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        displayPriority = .required
        titleVisibility = .hidden
        subtitleVisibility = .hidden
        glyphImage = UIImage(systemName: "lock.fill")
        glyphTintColor = .white
        markerTintColor = .systemBlue
    }
}
