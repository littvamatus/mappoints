//
//  MapAnnotationViewModel.swift
//  MapPoints
//
//  Created by Matus Littva on 04/07/2021.
//

import Foundation
import MapKit

struct MapAnnotationViewModel {
    let latitude: Double
    let longitude: Double
    let title: String
    let subtitle: String
}

extension MapAnnotationViewModel {
    
    init(store: Store) {
        self.latitude = store.latitude
        self.longitude = store.longitude
        self.title = store.name
        self.subtitle = [store.address, store.postalCode, store.city].joined(separator: ", ")
    }
}
