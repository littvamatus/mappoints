//
//  StoresMapViewModel.swift
//  MapPoints
//
//  Created by Matus Littva on 02/07/2021.
//

import Foundation
import CoreLocation

protocol StoresMapViewModelCoordinatorDelegate: AnyObject {
    func showDetail(store: Store, distance: Double)
}

final class StoresMapViewModel: NSObject, StoresMapConfigurable {

    private let storesProvider: StoresProvider
    private lazy var locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.delegate = self
        return manager
    }()
    private var stores: Set<Store> = [] {
        didSet {
            let newStores = stores.subtracting(oldValue)
            if !newStores.isEmpty {
                annotationsHandler?(newStores.map(MapAnnotationViewModel.init))
            }
        }
    }
    private var userLocation: CLLocationCoordinate2D = CLLocationCoordinate2D()
    
    weak var coordinatorDelegate: StoresMapViewModelCoordinatorDelegate?
    
    // MARK: - Output
    
    var errorHandler: ((_ title: String, _ message: String) -> Void)?
    var annotationsHandler: (([MapAnnotationViewModel]) -> Void)?
    var userCoordinatesHandler: ((_ latitude: Double, _ longitude: Double) -> Void)?
    
    init(storesProvider: StoresProvider) {
        self.storesProvider = storesProvider
    }
    
    // MARK: - Input
    
    func viewWillAppear() {
        locationManager.requestWhenInUseAuthorization()
    }
    
    func annotationSelected(latitude: Double, longitude: Double) {
        guard let store = stores.first(where: { $0.latitude == latitude && $0.longitude == longitude }) else { return }
        
        let distance = CLLocation(latitude: latitude, longitude: latitude).distance(from: CLLocation(coordinate: userLocation))
        coordinatorDelegate?.showDetail(store: store, distance: distance)
    }
    
    // MARK: - Private
    
    private func fetchStores(latitude: Double, longitude: Double) {
        storesProvider.stores(latitude: latitude, longitude: longitude) { [weak self] result in
            switch result {
            case .success(let stores):
                self?.stores = Set(stores)
            case .failure:
                self?.errorHandler?("Error", "Unable to load stores.")
            }
        }
    }
}

// MARK: - Location Manager Delegate

extension StoresMapViewModel: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let coordinate = locations.first?.coordinate else { return }
        
        locationManager.stopUpdatingLocation()
        userLocation = coordinate
        userCoordinatesHandler?(coordinate.latitude, coordinate.longitude)
        fetchStores(latitude: coordinate.latitude, longitude: coordinate.longitude)
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        switch manager.authorizationStatus {
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
        case .denied, .restricted:
            errorHandler?("Warning", "The application cannot display any results without location services enabled.")
        @unknown default:
            break
        }
    }
}
