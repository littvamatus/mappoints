//
//  StoresMapConfigurable.swift
//  MapPoints
//
//  Created by Matus Littva on 02/07/2021.
//

import Foundation

typealias StoresMapConfigurable = StoresMapViewModelInput & StoresMapViewModelOutput

protocol StoresMapViewModelInput {
    func viewWillAppear()
    func annotationSelected(latitude: Double, longitude: Double)
}

protocol StoresMapViewModelOutput {
    var errorHandler: ((_ title: String, _ message: String) -> Void)? { get set }
    var annotationsHandler: (([MapAnnotationViewModel]) -> Void)? { get set }
    var userCoordinatesHandler: ((_ latitude: Double, _ longitude: Double) -> Void)? { get set }
}
