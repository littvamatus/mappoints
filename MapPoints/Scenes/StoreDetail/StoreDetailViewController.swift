//
//  StoreDetailViewController.swift
//  MapPoints
//
//  Created by Matus Littva on 05/07/2021.
//

import UIKit
import PanModal
import AlamofireImage

final class StoreDetailViewController: UIViewController, PanModalPresentable {
    
    @IBOutlet private var logoImageView: UIImageView!
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var addressLabel: UILabel!
    @IBOutlet private var cityLabel: UILabel!
    @IBOutlet private var distanceLabel: UILabel!
    
    private var viewModel: StoreDetailConfigurable!
    
    var panScrollable: UIScrollView? { nil }
    var longFormHeight: PanModalHeight { .contentHeight(208) }
    var anchorModalToLongForm: Bool { false }
    var panModalBackgroundColor: UIColor { .clear }
    var cornerRadius: CGFloat { 24 }
    
    convenience init(viewModel: StoreDetailConfigurable) {
        self.init()
        self.viewModel = viewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupContent()
    }
    
    func panModalWillDismiss() {
        viewModel.detailWillDismiss()
    }
    
    // MARK: - Private
    
    private func setupView() {
        logoImageView.layer.cornerRadius = 10
    }
    
    private func setupContent() {
        if let imageURL = viewModel.imageURL {
            logoImageView.af.setImage(withURL: imageURL, imageTransition: .crossDissolve(0.2))
        } else {
            logoImageView.image = UIImage(systemName: viewModel.placeholderImageName)
        }
        nameLabel.text = viewModel.name
        addressLabel.text = viewModel.address
        cityLabel.text = viewModel.city
        distanceLabel.text = viewModel.distance
    }
    
    @IBAction private func navigate(_ sender: Any) {
        viewModel.navigate()
    }
}
