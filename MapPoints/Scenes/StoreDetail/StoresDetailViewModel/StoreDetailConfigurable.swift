//
//  StoresDetailConfigurable.swift
//  MapPoints
//
//  Created by Matus Littva on 05/07/2021.
//

import Foundation

typealias StoreDetailConfigurable = StoreDetailViewModelInput & StoreDetailViewModelOutput

protocol StoreDetailViewModelInput {
    func navigate()
    func detailWillDismiss()
}

protocol StoreDetailViewModelOutput {
    var placeholderImageName: String { get }
    var imageURL: URL? { get }
    var name: String { get }
    var address: String { get }
    var city: String { get }
    var distance: String { get }
}
