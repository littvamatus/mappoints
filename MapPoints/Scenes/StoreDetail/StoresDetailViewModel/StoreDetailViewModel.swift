//
//  StoreDetailViewModel.swift
//  MapPoints
//
//  Created by Matus Littva on 05/07/2021.
//

import Foundation

protocol StoreDetailViewModelCoordinatorDelegate: AnyObject {
    func navigateToSelectedStore()
    func storeDetailWillDismiss()
}

final class StoreDetailViewModel: StoreDetailConfigurable {
    
    private let store: Store
    private let distanceMeters: Double
    
    weak var coordinatorDelegate: StoreDetailViewModelCoordinatorDelegate?
    
    // MARK: - Output
    
    let placeholderImageName: String = "lock.shield"
    var imageURL: URL? { store.posTypeLogo }
    var name: String { store.name }
    var address: String { store.address }
    var city: String { [store.postalCode, store.city].joined(separator: ", ") }
    var distance: String {
        let distanceFormatter = MeasurementFormatter()
        distanceFormatter.unitOptions = .naturalScale
        let measurement = Measurement(value: distanceMeters, unit: UnitLength.micrometers)
        
        return distanceFormatter.string(from: measurement)
    }
    
    init(store: Store, distance: Double) {
        self.store = store
        self.distanceMeters = distance
    }
    
    // MARK: - Input
    
    func navigate() {
        coordinatorDelegate?.navigateToSelectedStore()
    }
    
    func detailWillDismiss() {
        coordinatorDelegate?.storeDetailWillDismiss()
    }
}
