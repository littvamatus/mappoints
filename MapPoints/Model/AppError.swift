//
//  AppError.swift
//  MapPoints
//
//  Created by Matus Littva on 01/07/2021.
//

import Foundation

struct AppError: Error, CustomStringConvertible {
    let description: String
}
