//
//  Store.swift
//  MapPoints
//
//  Created by Matus Littva on 01/07/2021.
//

import Foundation

struct Store: Decodable {
    let latitude: Double
    let longitude: Double
    let name: String
    let postalCode: String
    let address: String
    let city: String
    let posTypeLogo: URL?
    let distributorId: Int
}

extension Store: Equatable, Hashable {
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(distributorId)
        hasher.combine(latitude)
        hasher.combine(longitude)
    }
    
    static func == (lhs: Self, rhs: Self) -> Bool {
        lhs.distributorId == rhs.distributorId && lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude
    }
}
