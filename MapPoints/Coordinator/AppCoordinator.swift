//
//  AppCoordinator.swift
//  MapPoints
//
//  Created by Matus Littva on 02/07/2021.
//

import UIKit

final class AppCoordinator: Coordinator {
    
    private let window: UIWindow
    private let storesProvider: StoresProvider
    
    private var storesMapCoordinator: StoresMapCoordinator!
    
    init(window: UIWindow, storesProvider: StoresProvider) {
        self.window = window
        self.storesProvider = storesProvider
    }
    
    @discardableResult
    func start() -> UIViewController {
        storesMapCoordinator = StoresMapCoordinator(storesProvider: storesProvider)
        let mainViewController = storesMapCoordinator.start()
        window.rootViewController = mainViewController
        window.makeKeyAndVisible()
        return mainViewController
    }
}
