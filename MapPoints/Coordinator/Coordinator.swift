//
//  Coordinator.swift
//  MapPoints
//
//  Created by Matus Littva on 02/07/2021.
//

import UIKit

protocol Coordinator {
    func start() -> UIViewController
}
