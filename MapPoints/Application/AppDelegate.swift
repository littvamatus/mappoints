//
//  AppDelegate.swift
//  MapPoints
//
//  Created by Matus Littva on 01/07/2021.
//

import UIKit

@main
final class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    private let appCore = AppCore()
    private var coordinator: AppCoordinator!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        coordinator = AppCoordinator(window: window!, storesProvider: appCore.dependency())
        coordinator.start()
        return true
    }
}
