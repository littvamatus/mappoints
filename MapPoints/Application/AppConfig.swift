//
//  AppConfig.swift
//  MapPoints
//
//  Created by Matus Littva on 02/07/2021.
//

import Foundation

final class AppConfig {
    
    let logLevel: DebugLogger.LogLevel = .verbose
    
    lazy var apiKey: String = {
        loadValue(key: "ApiKey")
    }()
    
    lazy var apiBaseURL: String = {
        loadValue(key: "BaseApiURL")
    }()
    
    // MARK: - Private
    
    private func loadValue(key: String) -> String {
        guard let value = Bundle.main.object(forInfoDictionaryKey: key) as? String else {
            fatalError("Can't find \(key) in plist.")
        }
        return value
    }
}
