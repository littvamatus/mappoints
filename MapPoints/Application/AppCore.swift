//
//  AppCore.swift
//  MapPoints
//
//  Created by Matus Littva on 02/07/2021.
//

import Foundation

final class AppCore {
 
    private let config = AppConfig()
    
    init() {}
    
    func dependency() -> JSONDecoder {
        JSONDecoder()
    }
    
    func dependency(logLevel: DebugLogger.LogLevel) -> DebugLogger {
        DebugLogger(logLevel: logLevel)
    }
    
    func dependency() -> StoresProvider {
        StoresProvider(baseURL: config.apiBaseURL,
                       apiKey: config.apiKey,
                       debugLogger: dependency(logLevel: config.logLevel),
                       decoder: dependency())
    }
}
