//
//  CLLocation+Extensions.swift
//  MapPoints
//
//  Created by Matus Littva on 04/07/2021.
//

import CoreLocation

extension CLLocation {
    
    convenience init(coordinate: CLLocationCoordinate2D) {
        self.init(latitude: coordinate.latitude, longitude: coordinate.longitude)
    }
    
    func isEqual(to coordinate: CLLocationCoordinate2D, threshold: Double = 5) -> Bool {
        distance(from: CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)) < threshold
    }
}
