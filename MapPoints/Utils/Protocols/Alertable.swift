//
//  Alertable.swift
//  MapPoints
//
//  Created by Matus Littva on 04/07/2021.
//

import UIKit

protocol Alertable {}

extension Alertable where Self: UIViewController {
    
    func presentAlert(title: String,
                      message: String,
                      preferredStyle: UIAlertController.Style = .alert,
                      sourceView: UIView? = nil,
                      actions: [UIAlertAction] = [UIAlertAction(title: "OK", style: .default, handler: nil)]) {
        
        let alertViewController = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        actions.forEach { alertViewController.addAction($0) }
        
        if let sourceView = sourceView {
            alertViewController.popoverPresentationController?.sourceView = sourceView
            alertViewController.popoverPresentationController?.sourceRect = CGRect(origin: sourceView.center, size: .zero)
        }
        present(alertViewController, animated: true)
    }
}
