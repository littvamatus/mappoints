//
//  Provider.swift
//  MapPoints
//
//  Created by Matus Littva on 02/07/2021.
//

import Foundation
import Alamofire

protocol Provider {
    var baseURL: String { get }
    var apiKey: String { get }
    var debugLogger: DebugLogger { get }
    var decoder: JSONDecoder { get }
    
    func fetch<T: Decodable>(endpoint: Endpoint, decodingPath: String, completion: @escaping (Result<T, AppError>) -> Void)
}

extension Provider {
    var defaultHeaders: HTTPHeaders {
        [.accept("application/json"),
         .contentType("application/json")]
    }
    
    func fetch<T: Decodable>(endpoint: Endpoint, decodingPath: String, completion: @escaping (Result<T, AppError>) -> Void) {
        AF.request(endpoint: endpoint).decode(key: decodingPath, decoder: decoder, logger: debugLogger, completion: completion)
    }
}

private extension Alamofire.Session {
    
    @discardableResult
    func request(endpoint: Endpoint) -> DataRequest {
        request(endpoint,
                method: endpoint.method,
                parameters: endpoint.parameters,
                encoding: endpoint.encoding,
                headers: endpoint.headers).validate()
    }
}
