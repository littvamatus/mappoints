//
//  Endpoint.swift
//  MapPoints
//
//  Created by Matus Littva on 01/07/2021.
//

import Foundation
import Alamofire

struct Endpoint: URLConvertible {
    let baseURL: String
    let path: String
    let method: HTTPMethod
    let headers: HTTPHeaders
    let encoding: ParameterEncoding
    let parameters: Parameters?
    
    init(baseURL: String, path: String, method: HTTPMethod = .get, headers: HTTPHeaders, encoding: ParameterEncoding = URLEncoding.default, parameters: Parameters? = nil) {
        self.baseURL = baseURL
        self.path = path
        self.method = method
        self.headers = headers
        self.encoding = encoding
        self.parameters = parameters
    }
    
    func asURL() throws -> URL {
        try baseURL.asURL().appendingPathComponent(path)
    }
}
