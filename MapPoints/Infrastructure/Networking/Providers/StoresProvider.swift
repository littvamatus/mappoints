//
//  StoresProvider.swift
//  MapPoints
//
//  Created by Matus Littva on 01/07/2021.
//

import Foundation

struct StoresProvider: Provider {
    
    let baseURL: String
    let apiKey: String
    let debugLogger: DebugLogger
    let decoder: JSONDecoder
    
    init(baseURL: String, apiKey: String, debugLogger: DebugLogger, decoder: JSONDecoder) {
        self.baseURL = baseURL
        self.apiKey = apiKey
        self.debugLogger = debugLogger
        self.decoder = decoder
    }
    
    func stores(latitude: Double, longitude: Double, radius: Int = 1000, page: Int = 1, pageSize: Int = 10, completion: @escaping (Result<[Store], AppError>) -> Void) {
        let parameters: [String: Any] = ["latitude": latitude,
                                         "longitude": longitude,
                                         "radius": radius,
                                         "page": page,
                                         "pageSize": pageSize,
                                         "clientApplicationKey": apiKey]
        let endpoint = Endpoint(baseURL: baseURL, path: "/stores", headers: defaultHeaders, parameters: parameters)
        fetch(endpoint: endpoint, decodingPath: "stores", completion: completion)
    }
}
